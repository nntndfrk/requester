import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor(private httpService: HttpClient) { }

  Projects: any[] = [];

  ngOnInit() {
    this.httpService.get('./assets/data/projects.json').subscribe(
      data => {
        this.Projects = data as string [];	 // FILL THE ARRAY WITH DATA.
        // console.log(this.Projects[0]);
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

}
