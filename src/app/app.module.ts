import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { MatIconModule, MatToolbarModule, MatButtonModule, MatSidenavModule,
  MatListModule, MatTooltipModule, MatCardModule, MatDividerModule,
  MatFormFieldModule, MatInputModule, MatTabsModule, MatSlideToggleModule } from '@angular/material';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './shared/toolbar/toolbar.component';
import { SidenavComponent } from './shared/sidenav/sidenav.component';
import { RequestComponent } from './request/request.component';
import { ProjectsComponent } from './projects/projects.component';
import { TempTZComponent } from './temp-tz/temp-tz.component';

@NgModule({
  declarations: [
    AppComponent,
    RequestComponent,
    SidenavComponent,
    ToolbarComponent,
    ProjectsComponent,
    TempTZComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, HttpClientModule,
    // Flex-layout
    FlexLayoutModule,
    // Material
    MatToolbarModule, MatIconModule, MatButtonModule, MatSidenavModule,
    MatListModule, MatTooltipModule, MatCardModule, MatDividerModule,
    MatFormFieldModule, MatInputModule, MatTabsModule, MatSlideToggleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
